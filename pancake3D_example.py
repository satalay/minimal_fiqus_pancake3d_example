import platform
import os

from fiqus import MainFiQuS

# check if it is Windows, Linux or Mac and set the path to GetDP accordingly
if platform.system() == "Windows":
    # Windows
    getdp_path = os.path.join(
        os.path.dirname(__file__), "getdp", "windows", "getdp.exe"
    )
elif platform.system() == "Linux":
    # Linux
    getdp_path = os.path.join(os.path.dirname(__file__), "getdp", "linux", "getdp")
elif platform.system() == "Darwin":
    raise NotImplementedError("CERNGetdp is not available for Mac OS yet!")


# run FiQuS with pancake3Dexample_fiqus.yaml:
output_folder = "output"
MainFiQuS.MainFiQuS(
    GetDP_path=getdp_path,
    input_file_path="pancake3Dexample_fiqus.yaml",
    model_folder=output_folder,
)

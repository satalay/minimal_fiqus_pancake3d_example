# CERN GetDP

FiQuS uses a specific version of GetDP ([CERNGetDP](https://gitlab.cern.ch/steam/cerngetdp/)). CERNGetDP can be downloaded from [here](https://gitlab.cern.ch/steam/cerngetdp/-/releases).
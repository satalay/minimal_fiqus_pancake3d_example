# A minimal FiQuS/Pancake3D example

## Getting started

1.  Install Python 3.8
2.  Install `fiqus==2024.1.1` with either

    ```
    pip install fiqus==2024.1.1
    ```

    or

    ```
    pip install -r requirements.txt
    ```

3.  Run `pancake3D_example.py` with
    ```
    python pancake3D_example.py
    ```

## Suggestions

- To get suggestions for the input file in your IDE, make sure to use an editor that supports JSON Schema. JSON Schema will be loaded automatically since the input file ends with `_fiqus.yaml`.
- To visualize geometry files (`*.brep`), mesh files (`*.msh`), and solution files (`*.pos`); download [Gmsh](https://gmsh.info/).
